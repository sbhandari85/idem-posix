import copy

from tests.integration.states import run_sls

STATE_PWD = """
pwd:
  cmd.run:
    - cmd: pwd
"""


def test_simple_run(hub):
    managed_state = {}
    running = run_sls(STATE_PWD, managed_state=managed_state)
    ret = running["cmd_|-pwd_|-pwd_|-run"]
    assert ret["result"], ret["comment"]
    assert ret["old_state"] is None, ret["comment"]
    assert ret["new_state"]["retcode"] == 0, ret["comment"]
    assert ret["new_state"]["state"] == {}, ret["comment"]
    assert ret["new_state"]["stderr"] == "", ret["comment"]
    assert ret["new_state"]["stdout"], ret["comment"]
    enforced_state = copy.copy(managed_state["cmd_|-pwd_|-pwd_|-"])
    assert ret["new_state"] == enforced_state, ret["comment"]


def test_enforced_state(hub):
    managed_state = {}
    ret1 = run_sls(STATE_PWD, managed_state=managed_state)
    assert ret1, ret1["comment"]
    old_state1 = copy.copy(managed_state["cmd_|-pwd_|-pwd_|-"])
    ret2 = run_sls(STATE_PWD, managed_state=managed_state)
    assert ret2, ret2["comment"]
    old_state2 = copy.copy(managed_state["cmd_|-pwd_|-pwd_|-"])
    assert old_state1 == old_state2, ret2["comment"]


STATE_ECHO_JSON_LIST = """
echo:
  cmd.run:
    - cmd: echo '["1", "2", "3"]'
    - render_pipe: json
    - shell: true
"""


def test_render_pipe_list(hub):
    managed_state = {}
    running = run_sls(STATE_ECHO_JSON_LIST, managed_state=managed_state)
    ret = list(running.values())[0]
    assert ret["result"], ret["comment"]
    assert ret["old_state"] is None, ret["comment"]
    assert ret["new_state"]["state"] == ["1", "2", "3"], ret["comment"]
    key = next(iter(managed_state))
    enforced_state = copy.copy(managed_state[key])
    assert ret["new_state"] == enforced_state, ret["comment"]


STATE_ECHO_JSON_DICT = """
echo:
  cmd.run:
    - cmd: >
        echo '{"1": "2"}'
    - render_pipe: json
    - shell: true
"""


def test_render_pipe_dict(hub):
    managed_state = {}
    running = run_sls(STATE_ECHO_JSON_DICT, managed_state=managed_state)
    ret = list(running.values())[0]
    assert ret["result"], ret["comment"]
    assert ret["old_state"] is None, ret["comment"]
    assert ret["new_state"]["state"] == {"1": "2"}, ret["comment"]
    key = next(iter(managed_state))
    enforced_state = copy.copy(managed_state[key])
    assert ret["new_state"] == enforced_state, ret["comment"]


STATE_CMD_LIST = """
echo:
  cmd.run:
    - cmd:
        - ls
        - -l
        - -a
"""


def test_cmd_list(hub):
    managed_state = {}
    running = run_sls(STATE_ECHO_JSON_DICT, managed_state=managed_state)
    ret = list(running.values())[0]
    assert ret["result"], ret["comment"]
    assert ret["old_state"] is None, ret["comment"]
    assert ret["new_state"]["stderr"] == "", ret["comment"]
    assert ret["new_state"]["stdout"], ret["comment"]
    assert ret["new_state"]["retcode"] == 0, ret["comment"]
    key = next(iter(managed_state))
    enforced_state = copy.copy(managed_state[key])
    assert ret["new_state"] == enforced_state, ret["comment"]


STATE_FAILED_CMD = """
echo:
  cmd.run:
    - cmd: this_is_not_a_real_command
"""


def test_cmd_fail(idem_cli, tests_dir):
    running = idem_cli("state", tests_dir / "sls" / "fail.sls")
    ret = list(running.json.values())[0]
    assert not ret["result"], ret["comment"]
    assert ret["old_state"] is None, ret["comment"]
    assert ret["new_state"]["retcode"] == 2, ret["comment"]
    assert "FileNotFoundError" in ret["new_state"]["stderr"], ret["comment"]


STATE_NONZERO_CMD = """
false:
  cmd.run:
    - cmd: 'false'
"""


def test_cmd_non_zero_retcode_fails(idem_cli, tests_dir):
    managed_state = {}
    running = run_sls(STATE_NONZERO_CMD, managed_state=managed_state)
    ret = list(running.values())[0]
    assert not ret["result"], ret["comment"]
    assert ret["new_state"]["retcode"] == 1, ret["comment"]


STATE_NONZERO_CMD_EXPECTED = """
false:
  cmd.run:
    - cmd: 'false'
    - success_retcodes:
      - 1
"""


def test_cmd_non_zero_retcode_passes(idem_cli, tests_dir):
    managed_state = {}
    running = run_sls(STATE_NONZERO_CMD_EXPECTED, managed_state=managed_state)
    ret = list(running.values())[0]
    assert ret["result"], ret["comment"]
    assert ret["new_state"]["retcode"] == 1, ret["comment"]


def test_no_ctx_test(idem_cli, tests_dir):
    without_test = idem_cli("state", tests_dir / "sls" / "no_ctx_test.sls")
    assert without_test.json["cmd_|-no_ctx_|-no_ctx_|-run"]["result"] is True

    with_test = idem_cli("state", tests_dir / "sls" / "no_ctx_test.sls", "--test")
    assert with_test.json["cmd_|-no_ctx_|-no_ctx_|-run"]["result"] is True

    assert (
        without_test.json["cmd_|-no_ctx_|-no_ctx_|-run"]["new_state"]
        == with_test.json["cmd_|-no_ctx_|-no_ctx_|-run"]["new_state"]
    )
