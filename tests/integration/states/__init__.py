import json
import tempfile
import uuid

import pop.hub
import yaml


def run_sls(
    yaml_block: str, test: bool = False, name: str = "run_sls", managed_state=None
):
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")
    hub.pop.loop.create()
    sls_run = str(uuid.uuid4())
    if managed_state is None:
        managed_state = {}

    data = {f"{sls_run}.sls": yaml.safe_load(yaml_block)}

    coro = hub.idem.state.apply(
        name=name,
        sls_sources=[f"json://{json.dumps(data)}"],
        render="json",
        runtime="parallel",
        subs=["states"],
        cache_dir=tempfile.gettempdir(),
        sls=[sls_run],
        test=test,
        managed_state=managed_state,
    )
    try:
        hub.pop.loop.CURRENT_LOOP.run_until_complete(coro)
    finally:
        hub.pop.loop.CURRENT_LOOP.close()

    assert not hub.idem.RUNS[name]["errors"], "\n".join(hub.idem.RUNS[name]["errors"])
    return hub.idem.RUNS[name]["running"]
