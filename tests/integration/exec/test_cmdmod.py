import asyncio
import os

import pytest
from dict_tools import data


@pytest.mark.asyncio
async def test_run(hub):
    ret = await hub.exec.cmd.run("pwd")
    assert isinstance(ret, data.NamespaceDict)
    assert ret.result, ret.comment
    assert ret.ret.stdout.strip().split(), "No output from command"


@pytest.mark.asyncio
async def test_run_error(hub):
    ret = await hub.exec.cmd.run("nonexistent_command")
    assert isinstance(ret, data.NamespaceDict)
    assert not ret.result, ret.comment
    assert not ret.ret.stdout.strip()
    assert ret.ret.stderr.strip()


@pytest.mark.asyncio
async def test_run_string_list(hub):
    ret = await hub.exec.cmd.run("ls -la")
    assert isinstance(ret, data.NamespaceDict)
    assert ret.result, ret.comment
    assert ret.ret.stdout, "No output from command"


@pytest.mark.asyncio
async def test_run_list(hub):
    ret = await hub.exec.cmd.run(["ls", "-la"])
    assert isinstance(ret, data.NamespaceDict)
    assert ret.result, ret.comment
    assert ret.ret.stdout, "No output from command"


@pytest.mark.asyncio
async def test_shell(hub):
    homedir = os.path.expanduser("~")
    ret = await hub.exec.cmd.run("echo -n $HOME", shell=True)
    assert isinstance(ret, data.NamespaceDict)
    assert ret.result, ret.comment
    assert ret.ret.stdout == homedir


@pytest.mark.asyncio
async def test_run_cwd(hub):
    ret = await hub.exec.cmd.run(["pwd"], cwd="/")
    assert isinstance(ret, data.NamespaceDict)
    assert ret.result, ret.comment
    assert ret.ret.stdout.strip() == "/"


@pytest.mark.asyncio
async def test_run_cwd_fail(hub):
    try:
        await hub.exec.cmd.run(["pwd"], cwd="/nonexistant_path")
    except SystemError:
        return
    assert False, "Expected a system error with a pad path"


@pytest.mark.asyncio
async def test_run_env(hub):
    env = {"test_env_attr": "test_env_val", "another": "value"}
    ret = await hub.exec.cmd.run("env", env=env)
    assert isinstance(ret, data.NamespaceDict)
    assert ret.result, ret.comment
    assert ret.ret.stdout.strip() == ("\n".join(f"{k}={v}" for k, v in env.items()))


@pytest.mark.asyncio
async def test_run_timeout(hub):
    ret = await hub.exec.cmd.run("pwd", timeout=1)
    assert isinstance(ret, data.NamespaceDict)
    assert ret.result, ret.comment
    assert ret.ret.stdout.strip(), "No output from command"


@pytest.mark.asyncio
async def test_run_timeout_fail(hub):
    try:
        await hub.exec.cmd.run(["sleep", "5"], timeout=0)
    except asyncio.TimeoutError:
        return

    assert False, "Timeout should have been reached"


@pytest.mark.asyncio
async def test_run_umask(hub):
    ret = await hub.exec.cmd.run("pwd", umask="666")
    assert isinstance(ret, data.NamespaceDict)
    assert ret.result, ret.comment
    assert ret.ret.stdout.strip(), "No output from command"
