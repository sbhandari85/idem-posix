import socket
from unittest import mock

import pytest
from dict_tools import data


@pytest.mark.asyncio
async def test_load_localhost(hub, mock_hub):
    with mock.patch("socket.gethostname", return_value="test_host"):
        mock_hub.grains.posix.net.fqdn.load_localhost = (
            hub.grains.posix.net.fqdn.load_localhost
        )
        await mock_hub.grains.posix.net.fqdn.load_localhost()
    assert mock_hub.grains.GRAINS.localhost == "test_host"


@pytest.mark.asyncio
async def test_load_fqdn(mock_hub, hub):
    mock_hub.grains.GRAINS.localhost = "test_host"
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": "test.local"})
    ret = (
        (
            ({socket.AF_INET: 2}, {socket.SOCK_DGRAM: 2}, 17, "", ("127.0.0.1", 0)),
            ({socket.AF_INET: 2}, {socket.SOCK_STREAM: 1}, 6, "", ("127.0.0.1", 0)),
            (
                {socket.AF_INET: 2},
                {socket.SOCK_DGRAM: 2},
                17,
                "",
                ("192.168.1.24", 0),
            ),
            (
                {socket.AF_INET: 2},
                {socket.SOCK_STREAM: 1},
                6,
                "test.local",
                ("192.168.1.24", 0),
            ),
        ),
    )

    mock_hub.grains.posix.net.fqdn.load_fqdn = hub.grains.posix.net.fqdn.load_fqdn
    with mock.patch("socket.getaddrinfo", side_effect=ret):
        await mock_hub.grains.posix.net.fqdn.load_fqdn()

    assert mock_hub.grains.GRAINS.fqdn == "test.local"


@pytest.mark.asyncio
async def test_load_computer_name(mock_hub, hub):
    ...


@pytest.mark.asyncio
async def test_load_fqdns(mock_hub, hub):
    mock_hub.grains.GRAINS.fqdn = "test.local"
    mock_hub.grains.posix.net.fqdn.load_fqdns = hub.grains.posix.net.fqdn.load_fqdns
    ret = (
        (
            ({socket.AF_INET: 2}, {socket.SOCK_DGRAM: 2}, 17, "", ("127.0.0.1", 0)),
            ({socket.AF_INET: 2}, {socket.SOCK_STREAM: 1}, 6, "", ("127.0.0.1", 0)),
            (
                {socket.AF_INET: 2},
                {socket.SOCK_DGRAM: 2},
                17,
                "",
                ("192.168.1.24", 0),
            ),
            (
                {socket.AF_INET: 2},
                {socket.SOCK_STREAM: 1},
                6,
                "",
                ("192.168.1.24", 0),
            ),
        ),
        (
            (
                {socket.AF_INET6: 30},
                {socket.SOCK_DGRAM: 2},
                17,
                "",
                ("::1", 0, 0, 0),
            ),
            (
                {socket.AF_INET6: 30},
                {socket.SOCK_STREAM: 1},
                6,
                "",
                ("::1", 0, 0, 0),
            ),
            (
                {socket.AF_INET6: 30},
                {socket.SOCK_DGRAM: 2},
                17,
                "",
                ("fe80::1", 0, 0, 1),
            ),
            (
                {socket.AF_INET6: 30},
                {socket.SOCK_STREAM: 1},
                6,
                "",
                ("fe80::1", 0, 0, 1),
            ),
            (
                {socket.AF_INET6: 30},
                {socket.SOCK_DGRAM: 2},
                17,
                "",
                ("fe80::cac:ffff:ffff:ffff", 0, 0, 4),
            ),
            (
                {socket.AF_INET6: 30},
                {socket.SOCK_STREAM: 1},
                6,
                "",
                ("fe80::cac:ffff:ffff:ffff", 0, 0, 4),
            ),
        ),
    )
    with mock.patch("socket.getaddrinfo", side_effect=ret):
        await mock_hub.grains.posix.net.fqdn.load_fqdns()

    assert mock_hub.grains.GRAINS.domain == "local"

    # These ones require socket to be mocked
    assert mock_hub.grains.GRAINS.fqdn_ip4 == ("127.0.0.1", "192.168.1.24")
    assert mock_hub.grains.GRAINS.fqdn_ip6 == (
        "::1",
        "fe80::1",
        "fe80::cac:ffff:ffff:ffff",
    )
    assert mock_hub.grains.GRAINS.fqdns == (
        "127.0.0.1",
        "192.168.1.24",
        "::1",
        "fe80::1",
        "fe80::cac:ffff:ffff:ffff",
    )
